<?php

namespace WDRO\CurrencyConverter\Controller\Adminhtml\Converter\Ajax;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use OceanApplications\currencylayer\client;
use WDRO\CurrencyConverter\Api\ConvertManager;

/**
 * Class Check
 * @package WDRO\CurrencyConverter\Controller\Adminhtml\Converter\Ajax
 */
class Check extends Action
{
    /** @var JsonFactory  */
    protected $resultJsonFactory;

    /** @var ConvertManager  */
    protected $convertManager;

    /**
     * Check constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param ConvertManager $convertManager
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        ConvertManager $convertManager
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->convertManager = $convertManager;
        parent::__construct($context);
    }

    /**
     * Collect relations data
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultJsonFactory->create();

        try {
            $return = $this->convertManager
                ->setCurrency('RUB', 'PLN')
                ->setPrice($this->getRequest()->getParam('price'))
                ->convert();
        } catch (\Exception $e) {
            return $result->setData([
                'message' => $e->getMessage(),
                'success' => false
            ]);
        }

        return $result->setData([
                'success' => true,
                'price'   => $return
            ]);
    }
}