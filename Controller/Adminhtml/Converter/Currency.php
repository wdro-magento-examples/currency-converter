<?php

namespace WDRO\CurrencyConverter\Controller\Adminhtml\Converter;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Currency extends Action
{
    /** @var PageFactory  */
    protected $resultPageFactory;

    /**
     * Currency constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return  $resultPage;
    }
}