<?php

namespace WDRO\CurrencyConverter\Api;

use Magento\Framework\HTTP\ZendClient;

/**
 * Class ConvertManager
 *
 * Example uri: https://free.currencyconverterapi.com/api/v6/convert?q=RUB_PLN&compact=y
 *
 * @package WDRO\CurrencyConverter\Api
 */
class ConvertManager
{
    const API_URL = 'https://free.currencyconverterapi.com';
    const CONVERT_ENDPOINT = '/api/v6/convert';

    const CONPACT_TRUE = 'y';
    const CONPACT_FALSE = 'n';

    /** @var string  */
    private $from = '';

    /** @var string  */
    private $to = '';

    /** @var string  */
    private $compact = ConvertManager::CONPACT_TRUE;

    /** @var string  */
    private $price = '';

    /**
     * @param string $price
     * @return $this
     * @throws \Exception
     */
    public function setPrice(string $price)
    {
        if (preg_match('/^\d+(?:\.\d{2})?$/', $_POST['price']) == '0') {
            throw new \Exception(__("Invalide price (ex. '123.45')."), 409);
        }
        $this->price = $price;

        return $this;
    }

    /**
     * @param string $from
     * @param string $to
     * @return $this
     */
    public function setCurrency(string $from, string $to)
    {
        $this->from = $from;
        $this->to   = $to;

        return $this;
    }

    /**
     * @return $this
     */
    public function disableCompact()
    {
        $this->compact = self::CONPACT_FALSE;

        return $this;
    }

    /**
     * @return ZendClient
     */
    private function getZendClient()
    {
        return new ZendClient($this->getUrl() . $this->getParams());
    }

    /**
     * @return float
     */
    public function convert()
    {
        $zendClient = $this->getZendClient()->request(ZendClient::GET);
        $convert = json_decode($zendClient->getBody())->{$this->getCurrencyName()}->val;
        $oldPrice = floatval($this->ammount);
        return round($oldPrice * $convert, 2);
    }

    /**
     * @return string
     */
    private function getUrl()
    {
        return self::API_URL . self::CONVERT_ENDPOINT;
    }

    /**
     * @return string
     */
    private function getParams()
    {
        return ("?q={$this->getCurrencyName()}&compact={$this->compact}");
    }

    /**
     * @return string
     */
    private function getCurrencyName()
    {
        return ("{$this->from}_{$this->to}");
    }
}