# Install module

1) composer config repositories.wdro-magento-examples git https://gitlab.com/wdro-magento-examples/currency-converter.git

2) composer require wdro-magento-examples/currency-converter:1.0.0 

3) php bin/magento setup:upgrade

4) php bin/magento setup:static-content:deploy -f

### Support

![test](dock/img1.png)