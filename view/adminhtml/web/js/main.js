define([
    "jquery",
    'Magento_Ui/js/modal/alert',
    'mage/translate',
    "jquery/ui"
],function($, confirm, $t) {
    'use strict';
    $.widget('currency.action', {
        options: {
            url: '',
            method: 'POST',
            triggerEvent: 'click',
            form_key: null
        },
        _create: function() {
            this._bind();
        },
        _bind: function() {
            var self = this;
            self.element.on(self.options.triggerEvent, function() {
                var params = {
                    price: $("input#price_in").val(),
                    form_key: self.options.form_key
                };
                $.ajax({
                    showLoader: true,
                    url: self.options.url + "?isAjax=1",
                    data: params,
                    type: self.options.method,
                    dataType: 'json',
                    success: function(data, status, xhr) {
                        if (data.success === true && status == 'success') {
                            $("input#price_out").val(data.price);
                        } else if(data.success === false && status == 'success'){
                            confirm({
                                content: data.message
                            });
                        } else {
                            confirm({
                                content: $t('Something wrong!')
                            });
                        }
                    }
                });
            });
        }

    });
    return $.currency.action;
});