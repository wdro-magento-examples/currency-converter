<?php
/**
 * Created by PhpStorm.
 * User: Rafał Ossowski
 * Date: 05.08.2018
 * Time: 20:28
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'WDRO_CurrencyConverter',
    __DIR__
);